var nodemailer = require('nodemailer');
var Promise = require('promise');

exports.sendMail = (opts) => {
    var smtpTransport;

    console.log('Creating Transport');

    //smtp transport configuration
    var smtpTransport = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: process.env.USNB_EMAIL_USER,
            pass: process.env.USNB_EMAIL_PASSWORD
        }
    });

    //Message
    var message = {
        from: opts.from,
        replyTo: opts.from,
        to: opts.to,
        subject: opts.subject,
        html: opts.body
    };

    console.log('Sending Mail');
    // Send mail

    smtpTransport.sendMail(message, (error, info) => {
        if (error) {
            console.log(error);
        } else {
            console.log('Message sent successfully!');
            console.log('Server responded with "%s"', info.response);
        }
        console.log('Closing Transport');
        smtpTransport.close();
    });


}
