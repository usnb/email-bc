var endpoint = require('./amqp-endpoint');

var port = (process.env.USNB_EMAIL_PERSONA_PORT);

console.log("Running in :" + process.env.NODE_ENV);
console.log('About to start listening');
endpoint.listen(port);
console.log('Listening on port: ', port);
